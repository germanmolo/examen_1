package com.germanmolo.examentipoa;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class RectanguloActivity extends AppCompatActivity {

    private Rectangulo rectangulo;
    private EditText txtBase;
    private EditText txtAltura;
    private TextView saludo;
    private TextView resArea;
    private TextView resPerimetro;
    private Button calcular;
    private Button limpiar;
    private Button regresar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rectangulo_activity);

        txtAltura = (EditText)findViewById(R.id.txtAltura);
        txtBase= (EditText)findViewById(R.id.txtBase);
        resArea = (TextView)findViewById(R.id.lblArea);
        resPerimetro = (TextView)findViewById(R.id.lblPerimetro);
        saludo= (TextView)findViewById(R.id.lblNombre);
        calcular  = (Button)findViewById(R.id.btnCalcuar);
        limpiar = (Button)findViewById(R.id.btnLimpiar);
        regresar = (Button)findViewById(R.id.btnRegresar);

        Bundle datos =getIntent().getExtras();
        rectangulo =(Rectangulo)datos.getSerializable("datos");
        saludo.setText("Mi nombre es " + datos.getString("nombre"));

        calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            String alturaText = txtAltura.getText().toString();
            String baseText = txtBase.getText().toString();

            if(alturaText.isEmpty() || baseText.isEmpty()){
                Toast.makeText(RectanguloActivity.this, "Favor de introducir los datos necesarios", Toast.LENGTH_SHORT).show();
            }else{
                Integer altura = Integer.parseInt(txtAltura.getText().toString());
                Integer base = Integer.parseInt(txtBase.getText().toString());
                rectangulo.setAltura(altura);
                rectangulo.setBase(base);
                resArea.setText(String.valueOf(rectangulo.calcularArea()) + " unidades cuadradas");
                resPerimetro.setText(String.valueOf(rectangulo.calcularPerimetro()) + " unidades");
            }
            }
        });

        limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtAltura.setText("");
                txtBase.requestFocus();
                txtBase.setText("");
                resPerimetro.setText("");
                resArea.setText("");
            }
        });

        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RectanguloActivity.this,MainActivity.class);
                startActivity(intent);
                finishAffinity();
            }
        });

    }
}
