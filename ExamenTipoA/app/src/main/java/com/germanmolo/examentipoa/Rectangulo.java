package com.germanmolo.examentipoa;

import java.io.Serializable;

public class Rectangulo implements Serializable {

    private int base;
    private int altura;

    public Rectangulo(int base, int altura) {
        this.base = base;
        this.altura = altura;
    }

    public Rectangulo() {
    }

    public float calcularArea(){
        return this.base*this.altura;
    }

    public float calcularPerimetro(){
        return this.altura+this.altura+this.base+this.base;
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }
}
