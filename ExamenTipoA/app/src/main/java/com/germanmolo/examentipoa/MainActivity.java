package com.germanmolo.examentipoa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Rectangulo rectangulo;
    private EditText txtNombre;
    private Button entrar;
    private Button salir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre = (EditText)findViewById(R.id.txtNombre);
        entrar = (Button)findViewById(R.id.btnEntrar);
        salir = (Button)findViewById(R.id.btnSalir);

        rectangulo =new Rectangulo();

        entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();
                if(nombre.isEmpty()){
                    Toast.makeText(MainActivity.this, "Favor de introducir los dampos necesarios", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(MainActivity.this, RectanguloActivity.class);
                    intent.putExtra("nombre",nombre);
                    Bundle objeto = new Bundle();
                    objeto.putSerializable("datos",rectangulo);
                    intent.putExtras(objeto);
                    startActivity(intent);
                }
            }
        });

        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
